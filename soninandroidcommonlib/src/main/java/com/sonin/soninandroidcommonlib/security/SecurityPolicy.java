package com.sonin.soninandroidcommonlib.security;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;
import android.util.Base64;

import com.sonin.soninandroidcommonlib.utils.FilesystemUtils;

public class SecurityPolicy {
	
	public enum EncryptionType
	{
		NONE, XOR, AES
	}
	
	public enum AuthenticationType
	{
		NONE, HMAC_SHA2_256
	}
	
	private static SecurityPolicy 		securityPolicy;
	private static EncryptionType 		encryptionType;
	private static AuthenticationType 	authenticationType;
	
	private static String				encryptionKey;
	private static String				authenticationKey;
	
	
	public static void init(Context context, EncryptionType encryption, AuthenticationType authentication) {
		
		securityPolicy 		= new SecurityPolicy();
		encryptionType 		= encryption;
		authenticationType 	= authentication;
		
		if(encryptionType != EncryptionType.NONE)
		{
			FilesystemUtils.getStringFromAssets(context, "encryption_key.txt", new FilesystemUtils.onStringLoadedListener() {
				@Override
				public void onStringLoaded(String string) {
					encryptionKey = string;
				}
			});
		}
		
		if(authenticationType != AuthenticationType.NONE)
		{

			FilesystemUtils.getStringFromAssets(context, "ma_key.txt", new FilesystemUtils.onStringLoadedListener() {
				@Override
				public void onStringLoaded(String string) {
					authenticationKey = string;
				}
			});
		}
		
		
		
	}

	public static SecurityPolicy getSecurityPolicy() {

		if (securityPolicy == null) {
			throw new IllegalStateException(
					SecurityPolicy.class.getSimpleName()
							+ " is not initialized, call initializeInstance(..) method first.");
		}

		return securityPolicy;
	}
	
	
	public byte[] applyPolicy(String body) throws IOException, NoSuchAlgorithmException, InvalidKeyException
	{
		
		byte[] returnData = null;
		
		switch(encryptionType)
		{
		case NONE:
			
			returnData = body.getBytes("UTF-8");
			
			if(authenticationType==AuthenticationType.NONE)
			{
				return returnData;
			}
			
			break;
		case XOR:
			
			String xordBody = EncryptionUtils.xorMessage(body, encryptionKey);
			returnData = xordBody.getBytes("UTF-8");
			
			break;
		case AES:
			
			returnData = EncryptionUtils.aesEncrypt(body, encryptionKey, "NoPadding");
			
			break;
		}
		
		
		switch(authenticationType)
		{
		case NONE:
			
			break;
		case HMAC_SHA2_256:
			
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(EncryptionUtils.hexStringToByteArray(authenticationKey), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			
			byte[] macData = sha256_HMAC.doFinal(returnData);
			
	         
	        returnData = EncryptionUtils.combineByteArrays(returnData, macData);
			
			break;
		}
		
		return Base64.encode(returnData, Base64.NO_WRAP);
	}

}
