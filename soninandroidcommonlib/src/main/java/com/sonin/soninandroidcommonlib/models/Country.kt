package com.sonin.soninandroidcommonlib.models

import android.content.Context

/**
 * Created by athanasios.moutsioul on 09/10/2017.
 */
class Country{

    var mName:              String? = null
    var mCountryISO:        String? = null
    var mCountryCode:       Int = 0
    var mCountryCodeStr:    String? = null
    var mPriority:          Int = 0
    var mResId:             Int = 0
    var mNum:               Int = 0

    constructor( context: Context,  strCountryRecord : String,  index: Int){

        val arrData     = strCountryRecord.split(",")
        mNum            = index
        mName           = arrData[0]
        mCountryISO     = arrData[1]
        mCountryCode    = Integer.parseInt(arrData[2])
        mCountryCodeStr = "+" + arrData[2]

        if (arrData.size > 3) {

            mPriority   = Integer.parseInt(arrData[3])

        }
        val filename    =  String.format("f%03d", index)
        mResId          = context.getApplicationContext().getResources().getIdentifier(filename, "drawable", context.getApplicationContext().getPackageName())

    }

}