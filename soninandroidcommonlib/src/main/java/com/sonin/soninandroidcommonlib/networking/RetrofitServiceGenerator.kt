package com.sonin.soninandroidcommonlib.networking

import android.accounts.AccountManager
import android.app.Activity
import com.sonin.soninandroidcommonlib.accountManager.AccountGeneral
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.HashMap
import java.util.concurrent.TimeUnit

class RetrofitServiceGenerator {

    companion object {

        var strGrandType                = "password"
        var strGrandTypeRefreshToken    = "refresh_token"
        lateinit var retrofitObj        : Retrofit

        private val serviceMap : HashMap<Any, Object> = HashMap<Any, Object>();

        /**
         * Generated the Retrofit object for the login procedure.
         * Adds Logging interceptor
         * Adds header interceptor
         */
        fun <T> getServiceWithoutUsingAuthorization(clazz: Class<T>, strEndPointUrl: String, timeoutInSeconds :Long  = 5 ) : T  {

            val retrofitBuilderoAth : Retrofit.Builder = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(strEndPointUrl)
                    .addConverterFactory(MoshiConverterFactory.create());

            val okHttpBuilder       : OkHttpClient.Builder      =  OkHttpClient.Builder()
            val interceptorLogging  : HttpLoggingInterceptor =  HttpLoggingInterceptor()

            interceptorLogging.level = HttpLoggingInterceptor.Level.BODY

            val interceptor         : Interceptor = Interceptor {

                it.proceed(it.request().newBuilder().addHeader("Accept", "application/json").build())

            }

            okHttpBuilder.addInterceptor(interceptor)
            okHttpBuilder.addInterceptor(interceptorLogging).build();

            val okHttpClient    = okHttpBuilder.readTimeout(timeoutInSeconds,TimeUnit.SECONDS).writeTimeout(timeoutInSeconds,TimeUnit.SECONDS).build()

            val retrofit        = retrofitBuilderoAth.client(okHttpClient).build()

            if (!serviceMap.containsKey(clazz)) {

                serviceMap.put(clazz, retrofit.create(clazz) as Object)

            }

            return serviceMap[clazz] as T

        }

        /**
         *Generated a Retrofit object for the network requests.
         * Adds Logging interceptor
         * Adds authorization header
         * Adds okHttp Authenticator
         */
        fun <T> getServiceUsingAuthorization (clazz: Class<T>, activity : Activity, accessToken: String,strUserName : String, accountManager: AccountManager, strEndPointUrl: String, converter : Converter.Factory?, accountType: String, timeoutInSeconds :Long  = 5) : T{

            var responseConverter =  CustomMoshiConverterFactory.create() as Converter.Factory

            if (converter!=null){

                responseConverter = converter

            }

            val retrofitBuilder = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(strEndPointUrl)
                    .addConverterFactory(responseConverter);

            val interceptorLogging      = HttpLoggingInterceptor()
            interceptorLogging.level    = HttpLoggingInterceptor.Level.BODY
            val okHttpBuilder           = OkHttpClient.Builder()

            val strAuthorizationKey = accessToken

            //add headers
            val interceptor         : Interceptor = Interceptor {

                it.proceed(it.request().newBuilder()
                        .addHeader("Authorization", strAuthorizationKey)
                        .addHeader("Accept", "application/json").build())

            }



            accessToken?.let {

                okHttpBuilder.authenticator { route, response ->

                    val account = AccountGeneral.getAccountFromUsername(accountManager, strUserName, accountType)

                    account?.let { AccountGeneral.invalidateAuthToken(accountManager, it, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS) }

                    null
                }

            }

            okHttpBuilder.addNetworkInterceptor(interceptor);
            okHttpBuilder.addInterceptor(interceptorLogging).build();
            okHttpBuilder.cache(null);
            var okHttpClient    = okHttpBuilder.readTimeout(timeoutInSeconds,TimeUnit.SECONDS).writeTimeout(timeoutInSeconds,TimeUnit.SECONDS).build()
            var retrofit        = retrofitBuilder.client(okHttpClient).build()
            retrofitObj = retrofit
            return retrofit.create(clazz)
//            if (!serviceMap.containsKey(clazz)) {
//
//                serviceMap.put(clazz, retrofit.create(clazz) as Object)
//
//            }
//            return serviceMap[clazz] as T

        }

        /**
         * Generates a Generic Retrofit Object that doesn't need any Authorization header
         */
        fun <T> getRetrofitService (clazz: Class<T>, activity : Activity,strEndPointUrl: String ) : T{

            val testRetrofitBuilder = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(strEndPointUrl)
                    .addConverterFactory(GsonConverterFactory.create());

            val interceptorLogging      = HttpLoggingInterceptor()
            val okHttpBuilder           = OkHttpClient.Builder()
            okHttpBuilder.addInterceptor(interceptorLogging).build();
            val okHttpClient            = okHttpBuilder.build()
            val retrofit                = testRetrofitBuilder.client(okHttpClient).build()

            return retrofit.create(clazz as Class<T>)

        }

    }
}