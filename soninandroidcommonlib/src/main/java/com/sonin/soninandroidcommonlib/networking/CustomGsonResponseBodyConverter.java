package com.sonin.soninandroidcommonlib.networking;

/**
 * Created by athanasios.moutsioul on 24/01/2017.
 */

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.sonin.soninandroidcommonlib.utils.TangerineLog;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringReader;

import okhttp3.ResponseBody;
import retrofit2.Converter;

final class CustomGsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {

    private final Gson              gson;
    private final TypeAdapter<T>    adapter;

    CustomGsonResponseBodyConverter(Gson gson, TypeAdapter<T> adapter) {

        this.gson       = gson;
        this.adapter    = adapter;

    }


    @Override
    public T convert(ResponseBody responseBody) throws IOException {

        try {

            String strJsondata  = "";
            String strJson      = (String) responseBody.string();
            JSONObject json     = new JSONObject(strJson);

            String strSuccess   = json.getString("blnSuccess");

            TangerineLog.Companion.log("Network Response: " + strJson);

            if (strSuccess.equals("false")) {

                // the proper way is to get the error message through the json response and then throw the exception
                JSONArray jsonArray     = json.getJSONArray("arrErrors");
                JSONObject jsonError    = new JSONObject();
                jsonError.put("arrErrors",jsonArray);
                strJsondata             = jsonError.toString();

                throw new IOException(strJsondata);
            }


            if (json.has("objData")) {

                JSONObject jsonObject = json.getJSONObject("objData");
                strJsondata = jsonObject.toString();

            } else if (json.has("arrData")) {

                JSONArray jsonArray = json.getJSONArray("arrData");
                strJsondata = jsonArray.toString();

            } else {

                throw new IOException("There isn't any object or array inside the response!");

            }

            JsonReader jsonReader = gson.newJsonReader(new StringReader(strJsondata));
            try {

                return adapter.read(jsonReader);

            } finally {

                responseBody.close();

            }

        } catch (JSONException e) {

            throw new IOException("Failed to parse JSON", e);

        }
    }
}