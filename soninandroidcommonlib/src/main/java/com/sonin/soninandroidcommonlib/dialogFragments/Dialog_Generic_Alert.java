package com.sonin.soninandroidcommonlib.dialogFragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.sonin.soninandroidcommonlib.R;


public class Dialog_Generic_Alert extends DialogFragment implements
		OnClickListener {

	private TextView tvTitle;
	private TextView tvBody;
	private TextView tvSuccess;


	private static final String BUNDLE_KEY_TITLE  = "title";
	private static final String BUNDLE_KEY_BODY   = "body";
	private static final String BUNDLE_KEY_BUTTON = "button";



	public static Dialog_Generic_Alert newInstance(String title, String body, String buttonText) {
		
		Bundle bundle = new Bundle();
		bundle.putString(BUNDLE_KEY_TITLE,  title);
		bundle.putString(BUNDLE_KEY_BODY,   body);
		bundle.putString(BUNDLE_KEY_BUTTON, buttonText);

		Dialog_Generic_Alert f = new Dialog_Generic_Alert();
		f.setArguments(bundle);
		return f;
	}

	public Dialog_Generic_Alert() {
		// Empty constructor required for DialogFragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.dialog_fragment_alert, container);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		initVars(view);
		setData();
		return view;
	}
	


	

	private void setData() {
		
		Bundle bundle = getArguments();
		
		if(bundle!=null){
			
			if(bundle.containsKey(BUNDLE_KEY_TITLE)){
				tvTitle.setText(bundle.getString(BUNDLE_KEY_TITLE));
			}
			if(bundle.containsKey(BUNDLE_KEY_BODY)){
				tvBody.setText(bundle.getString(BUNDLE_KEY_BODY));
			}
			if(bundle.containsKey(BUNDLE_KEY_BUTTON)){
				tvSuccess.setText(bundle.getString(BUNDLE_KEY_BUTTON));
			}
		}
		
	}

	private void initVars(View view) {
		
		tvTitle    = (TextView) view.findViewById(R.id.tvDialogTitle);
		tvBody     = (TextView) view.findViewById(R.id.tvDialogBody);
		tvSuccess  = (TextView) view.findViewById(R.id.tvDialogButton);

		
		

		tvSuccess.setOnClickListener(this);
	}
	

	

	@Override
	public void onClick(View v) {
		v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		
		this.dismiss();

	}



	

}
