package com.sonin.soninandroidcommonlib.adapters;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.sonin.soninandroidcommonlib.R;


public class ContactsCursorAdapter extends CursorAdapter implements OnCheckedChangeListener {
	
	public int[] arrCheckedValues;

	
	public ContactsCursorAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
		
	}
	
	public void setListSize(int size){
		arrCheckedValues = new int[size];
	}
	
	public int[] getCheckedValues(){
		return arrCheckedValues;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		
		
		ViewHolder holder  =   (ViewHolder)    view.getTag();
        holder.tvContactName.setText(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
        
        holder.cbContactChecked.setTag(String.valueOf(cursor.getPosition()));
        
        if(arrCheckedValues[cursor.getPosition()]==1){
        	 holder.cbContactChecked.setChecked(true);	
        }else{
        	 holder.cbContactChecked.setChecked(false);	
        }
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		
				LayoutInflater inflater = ((Activity) context).getLayoutInflater();
				View view = inflater.inflate(R.layout.listitem_picker, parent, false);
	            ViewHolder holder          =   new ViewHolder();
	            holder.tvContactName       =   (TextView)  view.findViewById(R.id.tvContactIdentifier);
	            holder.cbContactChecked    =   (CheckBox)  view.findViewById(R.id.cbContactPicker);
	            holder.cbContactChecked.setOnCheckedChangeListener(this);
	            view.setTag(holder);
	            return view;
	}
	
	private static class ViewHolder {
		private TextView tvContactName;
		private CheckBox cbContactChecked;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		
		String cursorPosition = (String) buttonView.getTag();
		
		if(isChecked){
			arrCheckedValues[Integer.parseInt(cursorPosition)] = 1;
		}else{
			arrCheckedValues[Integer.parseInt(cursorPosition)] = 0;
		}
		
		
	}

}
