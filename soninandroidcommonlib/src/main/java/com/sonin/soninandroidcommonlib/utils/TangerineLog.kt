package com.sonin.soninandroidcommonlib.utils

import android.util.Log
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.info
import org.jetbrains.anko.wtf

open public class TangerineLog {

     companion object: AnkoLogger {

        private val DEFAULT_TAG             = "Tangerine Log:"
        private val NETWORK_REQUEST_TAG     = "Big Orange Networking Request:"
        private val NETWORK_RESPONSE_TAG    = "Big Orange Networking Response:"

         var deviceName     = android.os.Build.MODEL
         var deviceMan      = android.os.Build.MANUFACTURER

         enum class LogLevel {
             DEBUG, ERROR, WTF
         }

        /**
         * Logs provided message with default tag
         * @param strMessage
         */
        public fun log(strMessage: String) {

          if (deviceMan.equals("samsung")){

              info { strMessage }

          }else{

              wtf(strMessage)

          }

        }

         fun log(strMessage: String, strTag: String) {


             if (deviceMan.equals("samsung")){

                info { "$strTag : $strMessage"}

             }else{

                 Log.wtf(strTag, strMessage)

             }

         }

    }

    fun log(strMessage: String, level: LogLevel) {

            when (level) {

                LogLevel.DEBUG -> Log.d(DEFAULT_TAG, strMessage)
                LogLevel.ERROR -> Log.e(DEFAULT_TAG, strMessage)
                LogLevel.WTF -> Log.wtf(DEFAULT_TAG, strMessage)
                else -> Log.wtf(DEFAULT_TAG, strMessage)

            }

    }


}