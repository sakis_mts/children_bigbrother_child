package com.sonin.soninandroidcommonlib.customViews

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.Spinner
import com.sonin.soninandroidcommonlib.R
import android.widget.EditText
import com.sonin.soninandroidcommonlib.adapters.CountryAdapter
import com.sonin.soninandroidcommonlib.models.Country
import org.jetbrains.anko.doAsync
import android.R.attr.data
import android.content.res.TypedArray
import android.util.AttributeSet
import android.util.SparseArray
import android.view.View
import android.widget.AdapterView
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.sonin.soninandroidcommonlib.utils.PhoneUtils
import kotlinx.android.synthetic.main.phone_view.view.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import android.text.method.TextKeyListener.clear
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.sdk25.coroutines.onItemSelectedListener
import com.google.i18n.phonenumbers.NumberParseException
import com.sonin.soninandroidcommonlib.utils.CustomPhoneNumberFormattingTextWatcher
import com.sonin.soninandroidcommonlib.utils.PhoneUtils.CANADA_CODES
import com.sonin.soninandroidcommonlib.utils.PhoneUtils.DO_CODES
import com.sonin.soninandroidcommonlib.utils.PhoneUtils.PR_CODES
import com.sonin.soninandroidcommonlib.utils.PhoneUtils.US_CODES


class PhoneView(context: Context?, attrs: AttributeSet?) : LinearLayout(context, attrs) , AdapterView.OnItemSelectedListener {

    interface OnPhoneChangedListener {

        fun onPhoneChanged(phone: String)

    }

    var mAdapter                : CountryAdapter
    var mCountriesMap           : SparseArray<ArrayList<Country>>
    val mPhoneNumberUtil        = PhoneNumberUtil.getInstance()
    var mLastEnteredPhone       : String? = null
    var blnvalidateOnRuntime    : Boolean = false
    var blnLoadRegionFromPhone  : Boolean = false
    var strNumber          = ""

    lateinit var mOnPhoneChangedListener : OnPhoneChangedListener


    init {

        val inflater  = LayoutInflater.from(context);
        inflater.inflate(R.layout.phone_view, this);

        //get custom view attributes
         val a = context?.getTheme()?.obtainStyledAttributes(attrs, R.styleable.PhoneView, 0, 0);

        try {

            a?.let { blnvalidateOnRuntime =  it.getBoolean(R.styleable.PhoneView_autovalidation, false); }

        } finally {

            a?.recycle();

        }

        initPhoneChangedListener()

        mCountriesMap   = SparseArray<ArrayList<Country>>()
        mAdapter        = CountryAdapter(context);

        codeSpinner.setAdapter(mAdapter);
        codeSpinner.onItemSelectedListener = this

        etPhoneView.addTextChangedListener(CustomPhoneNumberFormattingTextWatcher(mOnPhoneChangedListener))
        initRegionCodes()
    }

    fun getSpinner()    : Spinner   = codeSpinner
    fun getEditText()   : EditText  = etPhoneView

    fun initRegionCodes(){

        async(UI) {

            initializeRegionCodesFromFiles()
            println(mCountriesMap.size())

        }
    }

    fun initializeRegionCodesFromFiles(){

        var  mSpinnerPosition   = -1;
        var arrData             = mutableListOf<Country>()
        val  reader             = BufferedReader(InputStreamReader(context.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"))

        try {
            var i = 0
            var  line: String? = null
            line = reader.readLine()

            while (line  != null) {
                //process line
                val c = Country(context, line, i)
                arrData.add(c)
                var list = mCountriesMap.get(c.mCountryCode)

                if (list == null) {

                    list = ArrayList<Country>()
                    mCountriesMap.put(c.mCountryCode, list)

                }

                list!!.add(c)
                i++
                line = reader.readLine()
            }

        } catch (e: IOException) {

            println(e.message)

        } finally {
            if (reader != null) {
                try {

                    reader!!.close()

                } catch (e: IOException) {
                    //log the exception
                }

            }
        }

        if (blnLoadRegionFromPhone){

            val countryRegion   = PhoneUtils.getCountryRegionFromPhone(context)
            val code            = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion)
            val list             = mCountriesMap.get(code)

            if (list != null) {

                for (c in list) {

                    if (c.mPriority == 0 || c.mPriority == 1) {

                        mSpinnerPosition = c.mNum
                        break

                    }
                }
            }

            mAdapter.addAll(arrData);
            if (mSpinnerPosition > 0) {

                codeSpinner.setSelection(mSpinnerPosition);

            }

        }else{

           setFlagToSpinner(strNumber,arrData)
            etPhoneView.setText(strNumber)


        }


    }

    fun reloadRegionCodesFromFiles(){

        var  mSpinnerPosition   = -1;
        var arrData             = mutableListOf<Country>()
        val  reader             = BufferedReader(InputStreamReader(context.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"))

        try {
            var i = 0
            var  line: String? = null
            line = reader.readLine()

            while (line  != null) {
                //process line
                val c = Country(context, line, i)
                arrData.add(c)
                var list = mCountriesMap.get(c.mCountryCode)

                if (list == null) {

                    list = ArrayList<Country>()
                    mCountriesMap.put(c.mCountryCode, list)

                }

                list!!.add(c)
                i++
                line = reader.readLine()
            }

        } catch (e: IOException) {

            println(e.message)

        } finally {
            if (reader != null) {
                try {

                    reader!!.close()

                } catch (e: IOException) {
                    //log the exception
                }

            }
        }
        val blnLoadRegionFromPhone = true
        if (blnLoadRegionFromPhone){

            val countryRegion   = PhoneUtils.getCountryRegionFromPhone(context)
            val code            = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion)
            val list            = mCountriesMap.get(code)

            if (list != null) {

                for (c in list) {

                    if (c.mPriority == 0 || c.mPriority == 1) {

                        mSpinnerPosition = c.mNum
                        break

                    }
                }
            }

            mAdapter.addAll(arrData);
            if (mSpinnerPosition > 0) {


                getSpinner().setSelection(mSpinnerPosition)

            }

        }else{

            setFlagToSpinner("",arrData)



        }


    }

    protected fun validate(): String? {

        var region  : String? = null
        var phone   : String? = null

        if (mLastEnteredPhone != null) {

            try {

                val p = mPhoneNumberUtil.parse(mLastEnteredPhone, null)
                val sb = StringBuilder(16)

                sb.append('+').append(p.countryCode).append(p.nationalNumber)
                phone = sb.toString()
                region = mPhoneNumberUtil.getRegionCodeForNumber(p)

            } catch (ignore: NumberParseException) {

            }

        }
        if (region != null) {

            return phone

        } else {

            return null

        }
    }

    fun setFlagToSpinner(strNumber: String, arrData: MutableList<Country>){


        var region  : String? = null
        var phone   : String? = null
        var  mSpinnerPosition   = -1;

        if (strNumber != null) {

            try {

                val p = mPhoneNumberUtil.parse(strNumber, null)
                val sb = StringBuilder(16)

                sb.append('+').append(p.countryCode).append(p.nationalNumber)
                phone = sb.toString()
                region = mPhoneNumberUtil.getRegionCodeForNumber(p)
                val code            = mPhoneNumberUtil.getCountryCodeForRegion(region)
                val list             = mCountriesMap.get(code)

                if (list != null) {

                    for (c in list) {

                        if (c.mPriority == 0 || c.mPriority == 1) {

                            mSpinnerPosition = c.mNum
                            break

                        }
                    }
                }

                mAdapter.addAll(arrData);
                if (mSpinnerPosition > 0) {

                    codeSpinner.setSelection(mSpinnerPosition);

                }

            } catch (ignore: NumberParseException) {

            }

        }
    }


    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        val c = codeSpinner.getItemAtPosition(position) as Country

        if (c.mCountryCodeStr!=null){
            val tmp = c.mCountryCodeStr!!

            mLastEnteredPhone?.let {
                if (it.startsWith(tmp)){

                    return

                }

            }

        }

        etPhoneView.text.clear()
        etPhoneView.text.insert(if (etPhoneView.text.length > 0) 1 else 0, c.mCountryCode.toString())
        etPhoneView.setSelection(etPhoneView.length())
    }

    fun initPhoneChangedListener(){

        mOnPhoneChangedListener = object: OnPhoneChangedListener{
            override fun onPhoneChanged(phone: String) {

                try {

                    mLastEnteredPhone = phone

                    val p = mPhoneNumberUtil.parse(phone, null)
                    val list = mCountriesMap.get(p.countryCode)
                    var country: Country? = null

                    if (list != null) {

                        if (p.countryCode == 1) {

                            val num = p.nationalNumber.toString()

                            if (num.length >= 3) {

                                val code = num.substring(0, 3)

                                if (CANADA_CODES.contains(code)) {

                                    for (c in list) {
                                        // Canada has priority 2, US has priority 1
                                        if (c.mPriority === 2) {
                                            country = c
                                            break
                                        }
                                    }
                                } else if (DO_CODES.contains(code)) {

                                    for (c in list) {

                                        // Dominican Republic has priority 3
                                        if (c.mPriority === 3) {

                                            country = c

                                            break
                                        }
                                    }
                                } else if (PR_CODES.contains(code)) {

                                    for (c in list) {
                                        // Puerto Rico has priority 4
                                        if (c.mPriority === 4) {
                                            country = c
                                            break
                                        }
                                    }
                                }
                            }
                        }
                        if (country == null) {
                            //If no country code is entered, default to US
                            for (c in list) {

                                if (c.mPriority === 1) {

                                    country = c
                                    break

                                }
                            }
                        }
                    }
                    if (country != null) {

                        val position = country.mNum
                        codeSpinner.post(Runnable { codeSpinner.setSelection(position) })

                    }

                    if (blnvalidateOnRuntime){

                        val phone = validate()
                        if (phone == null) {

                            etPhoneView.requestFocus();
                            etPhoneView.setError("incorrect");
                            return;
                        }

                    }



                } catch (ignore: NumberParseException) {
                }


            }
        }


    }

    fun validatePhoneNumber() : String? {

        val phone = validate()
        if (phone == null) {

            etPhoneView.requestFocus();
            etPhoneView.setError("incorrect");
        }
        return phone


    }

    fun setRegionFromPhone(){

        blnLoadRegionFromPhone = true
        initRegionCodes()

    }

    fun setRegionFromPhoneNumber(strPhoneNumber: String){

        blnLoadRegionFromPhone  = false
        strNumber               = strPhoneNumber
        initRegionCodes()
    }

}