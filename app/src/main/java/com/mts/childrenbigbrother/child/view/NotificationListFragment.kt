package com.mts.childrenbigbrother.child.view

import android.app.AlertDialog
import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mts.childrenbigbrother.child.R
import com.mts.childrenbigbrother.child.adapter.NotificationAdapter
import com.mts.childrenbigbrother.child.model.Notification
import com.sonin.soninandroidblankproject.database.AppDatabase
import kotlinx.android.synthetic.main.fragment_notification_list.*


/**
 * Created by sakis on 6/17/2018.
 */
class NotificationListFragment : Fragment() {

    lateinit var objNotificationAdapter: NotificationAdapter
    lateinit var arrNotification             : LiveData<List<Notification>>
    lateinit var appDatabase                 : AppDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (container == null) {
            return null
        }


        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_notification_list, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initVars()


    }



    fun initVars() {

        appDatabase         = AppDatabase.getDatabase(activity!!)
        arrNotification     = appDatabase.notificationModel().getAllNotification()

        objNotificationAdapter = NotificationAdapter(activity!!, mutableListOf<Notification>(), this::objClicked)
        rvNotification.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = objNotificationAdapter
        }

        setLiveDataObservers()

    }

    fun setLiveDataObservers() {

        arrNotification.observe(this, Observer {

            it?.let { notifications ->

                objNotificationAdapter.refreshData(notifications)
            }


        })



    }

    fun objClicked(notification: Notification, index:Int){


    }





}