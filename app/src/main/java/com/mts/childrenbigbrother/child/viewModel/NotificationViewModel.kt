package com.mts.childrenbigbrother.child.viewModel

import android.databinding.ObservableField

/**
 * Created by sakis on 5/27/2018.
 */
class NotificationViewModel {

    var strNotificationMessage      = ObservableField<String>()
    var notificationType            = ObservableField<String>()
    var notificationPerson          = ObservableField<String>()

}