package com.sonin.soninandroidblankproject.networking


import com.mts.childrenbigbrother.child.model.Child
import com.mts.childrenbigbrother.child.model.ChildFullDetails
import com.mts.childrenbigbrother.child.model.ChildLocationUpdates
import com.mts.childrenbigbrother.child.model.Notification

import io.reactivex.Single
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*

/**
 * Created by athanasios.moutsioul on 19/10/2017.
 */
interface NetworkRequests {

//    @POST("api/createChild")
//    fun createChild(@Body child: Child): Single<Child>
//
//    @GET("api/getChildren")
//    fun getChildren(): Observable<List<Child>>
//
//    @POST("api/testPush")
//    fun sendPush(@Body requestBody: RequestBody): Single<ResponseBody>
//
//    @POST("api/sendPush")
//    fun sendPush(@Body notification: Notification): Single<ResponseBody>
//
//    @POST("api/registerFirebaseToken")
//    fun registerFirebaseToken(@Body requestBody: RequestBody): Single<ResponseBody>
//
    @GET("api/getUser")
    fun getUser( ): Single<Child>


    @FormUrlEncoded
    @POST("api/childLogin")
    fun childLogin(@Field("code") code :String ): Single<ChildFullDetails>

    @POST("api/registerFirebaseToken")
    fun registerFirebaseToken(@Body requestBody: RequestBody): Single<ResponseBody>

    @POST("api/sendPushWithoutAuth")
    fun sendPush(@Body notification: Notification): Single<ResponseBody>

    @POST("api/createTransmitionWithoutAuth")
    fun sendTransitionUpdate(@Body locationUpdate: ChildLocationUpdates): Single<ResponseBody>
}