package com.mts.childrenbigbrother.child.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

class ChildLocationUpdates(
        val code        : String,
        val child_id    : Int,
        val latitude    : String,
        val longitude   : String,
        val date        : Long

        ): Serializable {
}