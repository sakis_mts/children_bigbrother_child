package com.mts.childrenbigbrother.child.view

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import com.mts.childrenbigbrother.child.R
import com.mts.childrenbigbrother.child.databinding.ActivityLoginBinding
import com.mts.childrenbigbrother.child.viewModel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.intentFor


class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    lateinit var loginFragmentBinding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val blnLoggedIn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("Logged", false)
        if (blnLoggedIn){
            startActivity(intentFor<HomeActivity>())

        }

        initViewModel()
        initBinding()
        initVars()

    }

    fun initVars(){

        btnLogin.setOnClickListener {

            loginViewModel.loginRequest()
        }

    }

    fun initBinding(){

        loginFragmentBinding = DataBindingUtil.setContentView(this,  R.layout.activity_login)
        loginFragmentBinding.loginViewModel = loginViewModel

    }

    fun initViewModel(){

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        loginViewModel.initViewModel(this)

    }

    override fun onResume() {
        super.onResume()
        loginViewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        loginViewModel.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        loginViewModel.onDestroy()
    }
}
