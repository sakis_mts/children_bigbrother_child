package com.sonin.soninandroidblankproject.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.mts.childrenbigbrother.child.model.Parent


/**
 * Created by athanasios.moutsioul on 11/04/2018.
 */
@Dao
interface ParentDao {

    @Query("select * from Parent")
    fun getParent(): LiveData<Parent>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addParent(parent: Parent)

    @Delete
    fun deleteParent(parent: Parent)

    @Query("DELETE FROM Parent")
    fun deleteParent()
}