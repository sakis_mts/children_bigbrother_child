package com.mts.childrenbigbrother.child.utils

import java.text.SimpleDateFormat
import java.util.*

fun Long.getDateTime(format: String? = null): String? {
    try {
        val sdf = SimpleDateFormat(format ?: "dd MMM yyyy HH:mm")
        val netDate = Date(this*1000L)
        return sdf.format(netDate)
    } catch (e: Exception) {
        return e.toString()
    }
}