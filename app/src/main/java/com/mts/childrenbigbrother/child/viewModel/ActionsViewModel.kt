package com.mts.childrenbigbrother.child.viewModel

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import com.google.firebase.iid.FirebaseInstanceId
import com.mts.childrenbigbrother.child.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.child.model.Notification
import com.mts.childrenbigbrother.child.model.RetrofitErrorResponse
import com.sonin.soninandroidblankproject.database.AppDatabase
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.jetbrains.anko.toast
import org.json.JSONObject
import javax.inject.Inject

class ActionsViewModel(application: Application) : AndroidViewModel(application) {


    lateinit var activity           : Activity
    var fragment                    : Fragment? = null
    val appDatabase                 : AppDatabase
    private var compositeDisposable = CompositeDisposable()

    @Inject
    lateinit var netService: NetworkRequests

    init {
        ChildrenBigBrotherApp.instance?.retrofitComponent?.inject(this)
        appDatabase         = AppDatabase.getDatabase(this.getApplication<Application>())
    }
    fun initViewModel(activity: Activity, fragment: Fragment? = null){

        this.activity       = activity
        this.fragment       = fragment



    }

    fun registerFirebaseTokenRequest(){
        createRequestBody()?.let {

            compositeDisposable.add(
                    registerFirebaseToken(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe ({
                                result ->

                                println("Firebase"+result)

                            }, { error ->
                                println(error)
                                RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                                error.printStackTrace()
                            })

            )

        }


    }

    fun sendPushRequest(notification: Notification){

        notification.insertItem(appDatabase)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe ({
                    result ->
                    notification.notificationId = result

                    compositeDisposable.add(
                            sendPush(notification)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe ({
                                        result ->

                                    }, { error ->
                                        println(error)
                                        notification.blnFailed = true
                                        notification.updateItem(appDatabase)
                                        RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                                        error.printStackTrace()
                                    })

                    )
                })






    }

    fun createRequestBodyForPush(): RequestBody?{
        var prefs = PreferenceManager.getDefaultSharedPreferences(activity)

        val userId =  prefs.getInt("userId",-1)
        if (userId != -1) {

            var jsonParams = JSONObject()
            FirebaseInstanceId.getInstance()?.let {
                it.token?.let { token->

                    if (!token.isEmpty()){

                        jsonParams.put("userId", userId)

                        return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(jsonParams).toString());
                    }
                }

            }
        }
        return null
    }

    fun createRequestBody(): RequestBody?{

        if (ChildrenBigBrotherApp.user.child.id != -1) {

            var jsonParams = JSONObject()
            FirebaseInstanceId.getInstance()?.let {
                it.token?.let { token->

                    if (!token.isEmpty()){

                        jsonParams.put("token", token )
                        jsonParams.put("userId", ChildrenBigBrotherApp.user.child.id)
                        jsonParams.put("userType","child")
                        return RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(jsonParams).toString());
                    }
                }

            }
        }
        return null
    }

    fun registerFirebaseToken(requestBody: RequestBody): Single<ResponseBody> {

        return netService.registerFirebaseToken(requestBody)
    }

    fun sendPush(notification: Notification): Single<ResponseBody> {

        return  netService.sendPush(notification)

    }

}