package com.mts.childrenbigbrother.child.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.sonin.soninandroidblankproject.database.AppDatabase
import org.jetbrains.anko.doAsync
import java.io.Serializable

/**
 * Created by sakis on 3/18/2018.
 */
@Entity(tableName = "Child")
data class Child(

        @PrimaryKey(autoGenerate = false)
        var id: Int? = null,
        var strName: String,
        var strPhone: String,
        var strAge: String,
        var codes : String,
        var blnMale: Int,
        var parentId: Int

): Serializable {
    constructor() : this(-1,"","","", "",0,-1)

   fun deleteItem(appDatabase : AppDatabase) {

       doAsync {
           appDatabase.childModel().deleteChild(this@Child)
       }

   }

    fun insertItem(appDatabase : AppDatabase){

        doAsync {
            appDatabase.childModel().addChild(this@Child)
        }

       
    }
}