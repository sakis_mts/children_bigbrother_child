package com.mts.childrenbigbrother.child.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.sonin.soninandroidblankproject.database.AppDatabase
import org.jetbrains.anko.doAsync
import java.io.Serializable

/**
 * Created by sakis on 3/18/2018.
 */
@Entity(tableName = "Parent")
data class Parent(

        @PrimaryKey(autoGenerate = false)
        var id: Int? = null,
        var name: String,
        var phone: String

): Serializable {
    constructor() : this(-1,"","")
//    constructor(id: Int? = null, strName: String,  strAge: String,  blnMale: Boolean) : this(strName, strAge, blnMale)
   fun deleteItem(appDatabase : AppDatabase) {
       // deleteAsyncTask(appDatabase).execute(this)
       doAsync {
           appDatabase.parentModel().deleteParent(this@Parent)
       }

   }

    fun insertItem(appDatabase : AppDatabase){

        doAsync {
            appDatabase.parentModel().addParent(this@Parent)
        }

        // insertAsyncTask(appDatabase).execute(this)
    }
}