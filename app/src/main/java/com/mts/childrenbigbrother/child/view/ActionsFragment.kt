package com.mts.childrenbigbrother.child.view

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.iid.FirebaseInstanceId

import com.mts.childrenbigbrother.child.R
import com.mts.childrenbigbrother.child.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.child.model.Notification
import com.mts.childrenbigbrother.child.services.FirebaseIDService
import com.mts.childrenbigbrother.child.viewModel.ActionsViewModel
import kotlinx.android.synthetic.main.fragment_actions.*
import org.jetbrains.anko.support.v4.defaultSharedPreferences
import org.json.JSONObject
import android.content.Intent.ACTION_CALL
import android.content.pm.PackageManager
import android.os.Build
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.util.Log
import com.mts.childrenbigbrother.child.BuildConfig



class ActionsFragment : Fragment(), View.OnClickListener {


    private  lateinit var actionsViewModel        : ActionsViewModel
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 1


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initVars()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        initViewModel()


        return inflater.inflate(R.layout.fragment_actions, container, false)
    }

    fun initViewModel(){

        actionsViewModel = ViewModelProviders.of(this).get(ActionsViewModel::class.java)
        actionsViewModel.initViewModel(activity!!,this)

    }

    fun  initVars(){

        if (ChildrenBigBrotherApp.user.child.id!= -1){

            println(FirebaseInstanceId.getInstance().token)
            if (defaultSharedPreferences.getBoolean(FirebaseIDService.BDK_REFRESHED_TOKEN, false)){
                actionsViewModel.registerFirebaseTokenRequest()
            }


        }

        rlPickMeUp.setOnClickListener(this)
        rlCall.setOnClickListener(this)
        rlLate.setOnClickListener(this)

    }

    override fun onClick(view: View) {

        val misc = JSONObject()
        misc.put("userId",ChildrenBigBrotherApp.user.parent.id )
        misc.put("type","parent" )
        misc.put("senderName",ChildrenBigBrotherApp.user.child.strName )

        when(view.id){
            R.id.rlPickMeUp -> {

                val objNotification = Notification(notificationType = Notification.OUTGOING,strMessage = activity?.getString(R.string.notif_body_pickmeup)!!,strMisc = misc.toString(),strChildName =  ChildrenBigBrotherApp.user.child.strName ?: "", dateCreated = System.currentTimeMillis()/1000)
                sendNotification(objNotification)

            }
            R.id.rlLate     -> {

                val objNotification = Notification(notificationType = Notification.OUTGOING,strMessage = activity?.getString(R.string.notif_body_late)!!,strMisc = misc.toString(),strChildName =  ChildrenBigBrotherApp.user.child.strName ?: "", dateCreated = System.currentTimeMillis()/1000)
                sendNotification(objNotification)

            }
            R.id.rlCall     -> {

                if (!checkPermissions()) {
                    requestPermissions();
                } else {
                    placeCall()
                }

            }

        }
    }


    fun sendNotification(notification: Notification){

        actionsViewModel.sendPushRequest(notification)
    }

    private fun checkPermissions(): Boolean {
        return PackageManager.PERMISSION_GRANTED === ActivityCompat.checkSelfPermission(activity!!,
                Manifest.permission.CALL_PHONE)
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                Manifest.permission.CALL_PHONE)

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {

            Snackbar.make(
                    this.view!!.findViewById(R.id.main_content),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, object : View.OnClickListener {
                        override fun onClick(view: View) {
                            // Request permission
                            ActivityCompat.requestPermissions(activity!!,
                                    arrayOf(Manifest.permission.CALL_PHONE),
                                    REQUEST_PERMISSIONS_REQUEST_CODE)
                        }
                    })
                    .show()
        } else {

            ActivityCompat.requestPermissions(activity!!,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(requestCode: Int,  permissions: Array<String>,
                                            grantResults: IntArray) {

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
              placeCall()


            } else {
                // Permission denied.
                Snackbar.make(
                        this.view!!.findViewById(R.id.main_content),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, object : View.OnClickListener {
                            override fun onClick(view: View) {
                                // Build intent that displays the App settings screen.
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null)
                                intent.data = uri
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent)
                            }
                        })
                        .show()
            }
        }
    }

    fun placeCall(){
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ChildrenBigBrotherApp.user.parent.phone))
        startActivity(intent)

    }
}
