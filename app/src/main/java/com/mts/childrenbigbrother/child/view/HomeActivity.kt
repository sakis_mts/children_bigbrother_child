package com.mts.childrenbigbrother.child.view

import android.Manifest
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.location.Location
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.view.Menu
import android.view.MenuItem
import com.mts.childrenbigbrother.child.R
import com.sonin.soninandroidcommonlib.activities.BaseActivityWithGoogleClient
import kotlinx.android.synthetic.main.activity_home.*
import android.os.Build.VERSION_CODES.O
import android.os.Build.VERSION.SDK_INT
import com.mts.childrenbigbrother.child.services.LocationUpdateForegroundService
import org.jetbrains.anko.intentFor
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.support.design.widget.Snackbar
import android.support.v4.view.accessibility.AccessibilityEventCompat.setAction
import android.R.string.ok
import android.net.Uri
import android.provider.Settings
import android.util.Log
import android.view.View
import com.mts.childrenbigbrother.child.BuildConfig


class HomeActivity : AppCompatActivity() ,TabLayout.OnTabSelectedListener{


    lateinit var adapter                : PagerAdapter
    private val TAG                     = "Permissions"
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initToolbar()
        initTablayout()
        initVars()
    }
    fun initToolbar(){

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false);
        supportActionBar?.setHomeButtonEnabled(false);
        supportActionBar?.title = ""


        // create our manager instance after the content view is set

    }

    fun initVars(){

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            if (Build.VERSION.SDK_INT === Build.VERSION_CODES.O) {

                startForegroundService(intentFor<LocationUpdateForegroundService>())

            } else {
                startService(intentFor<LocationUpdateForegroundService>())
            }
        }



    }


    private fun initTablayout() {

        tabs.addTab(tabs.newTab())
        tabs.addTab(tabs.newTab())
        tabs.getTabAt(0)?.setIcon(R.drawable.actions)
        tabs.getTabAt(1)?.setIcon(R.drawable.ic_action_map_icon2)
        tabs.getTabAt(0)?.icon?.setColorFilter(ContextCompat.getColor(this,R.color.accent), PorterDuff.Mode.SRC_IN)

//        tabs.getTabAt(1)?.icon?.setColorFilter(ContextCompat.getColor(this,R.color.black), PorterDuff.Mode.SRC_IN)
        tabs.setTabGravity(TabLayout.GRAVITY_FILL)

        //changeTabsFont()

        adapter = PagerAdapter(supportFragmentManager, tabs.getTabCount())
        viewPager.setAdapter(adapter)
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        viewPager.setOffscreenPageLimit(2);
        tabs.addOnTabSelectedListener(this)

    }

    private fun changeTabsFont() {

//        val vg = tabs.getChildAt(0) as ViewGroup
//        val tabsCount = vg.childCount
//
//        for (j in 0 until tabsCount) {
//
//            val vgTab = vg.getChildAt(j) as ViewGroup
//            val tabChildsCount = vgTab.childCount
//
//            for (i in 0 until tabChildsCount) {
//
//                val tabViewChild = vgTab.getChildAt(i)
//                if (tabViewChild is TextView) {
//                    tabViewChild.setTextSize(TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.edittext_text_size_low))
//                    tabViewChild.typeface = ResourcesCompat.getFont(this, R.font.avenir_heavy);
//                }
//
//            }
//        }
    }


    inner class PagerAdapter(fm: FragmentManager, internal var mNumOfTabs: Int) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {

            when (position) {
                0 -> {  return ActionsFragment() }
                1 -> {  return NotificationListFragment() }
                else -> return null
            }
        }

        override fun getCount(): Int {
            return mNumOfTabs
        }
    }


    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
        tab?.icon?.setColorFilter(ContextCompat.getColor(this,R.color.icons), PorterDuff.Mode.SRC_IN)
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        viewPager.setCurrentItem(tab?.getPosition()!!);
        tab?.icon?.setColorFilter(ContextCompat.getColor(this,R.color.accent), PorterDuff.Mode.SRC_IN)
    }

    private fun checkPermissions(): Boolean {
        return PackageManager.PERMISSION_GRANTED === ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            Snackbar.make(
                    findViewById(R.id.main_content),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, object : View.OnClickListener {
                        override fun onClick(view: View) {
                            // Request permission
                            ActivityCompat.requestPermissions(this@HomeActivity,
                                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    REQUEST_PERMISSIONS_REQUEST_CODE)
                        }
                    })
                    .show()
        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(this@HomeActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(requestCode: Int,  permissions: Array<String>,
                                             grantResults: IntArray) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                if (Build.VERSION.SDK_INT === Build.VERSION_CODES.O) {

                    startForegroundService(intentFor<LocationUpdateForegroundService>())

                } else {
                    startService(intentFor<LocationUpdateForegroundService>())
                }


            } else {
                // Permission denied.
                Snackbar.make(
                        findViewById(R.id.main_content),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, object : View.OnClickListener {
                            override fun onClick(view: View) {
                                // Build intent that displays the App settings screen.
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null)
                                intent.data = uri
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent)
                            }
                        })
                        .show()
            }
        }
    }
}

