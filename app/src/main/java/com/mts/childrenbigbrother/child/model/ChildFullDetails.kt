package com.mts.childrenbigbrother.child.model

data class ChildFullDetails(val child: Child, val parent: Parent) {
}