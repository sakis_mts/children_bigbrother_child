package com.sonin.soninandroidblankproject.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.mts.childrenbigbrother.child.database.ChildDao
import com.mts.childrenbigbrother.child.model.Child
import com.mts.childrenbigbrother.child.model.Notification
import com.mts.childrenbigbrother.child.model.Parent
import com.mts.childrenbigbrother.database.NotificationDao
import com.sonin.soninandroidcommonlib.utils.TangerineLog

@Database(entities = arrayOf( Parent::class,Child::class, Notification::class), version = 1)
abstract class AppDatabase : RoomDatabase() {



    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "children_bb_db").build()
            }
            TangerineLog.log("dbInstance: ${INSTANCE}")
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

    abstract fun parentModel()          : ParentDao
    abstract fun childModel()           : ChildDao
    abstract fun notificationModel()    : NotificationDao


}