package com.mts.childrenbigbrother.child.viewModel

import android.app.Activity
import android.app.Application

import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import android.preference.PreferenceManager

import android.support.v4.app.Fragment
import android.view.View
import com.google.gson.Gson
import com.mts.childrenbigbrother.child.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.child.application.ChildrenBigBrotherApp.Companion.BDK_USER
import com.mts.childrenbigbrother.child.model.ChildFullDetails
import com.mts.childrenbigbrother.child.model.RetrofitErrorResponse
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.toast
import javax.inject.Inject

/**
 * Created by sakis on 2/10/2018.
 */
class LoginViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var activity           : Activity
    var fragment                    : Fragment? = null
    var obsProgrBar                 = ObservableField(View.GONE)
    private var compositeDisposable = CompositeDisposable()
    var obsCode                     = ObservableField("RQANSw")


    @Inject
    lateinit var netService: NetworkRequests

    init {
        ChildrenBigBrotherApp.instance?.retrofitComponent?.inject(this)
    }
    fun initViewModel(activity: Activity, fragment: Fragment? = null){

        this.activity       = activity
        this.fragment       = fragment


    }


    fun getAccessToken(code: String) : Single<ChildFullDetails> {

        return netService.childLogin(code)

    }

    fun loginRequest(){

        obsProgrBar.set(View.VISIBLE)
        compositeDisposable.add(
                getAccessToken(obsCode.get()!!)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->

                            obsProgrBar.set(View.GONE)
                            SessionManager.instance.login(activity,true,true)
                            var prefs = PreferenceManager.getDefaultSharedPreferences(activity)
                            prefs.edit().putString(BDK_USER, Gson().toJson(result)).commit()
                            PreferenceManager.getDefaultSharedPreferences(activity).edit().putBoolean("Logged", true).commit()

                        }, { error ->


                            RetrofitErrorResponse.onError(error)?.let { activity.toast(it) }
                           println(error)
                            error.printStackTrace()
                            obsProgrBar.set(View.GONE)
                        })


        )


    }


    fun onResume(){

        compositeDisposable = CompositeDisposable()

    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

    fun onPause() {
        obsProgrBar.set(View.GONE)

    }
}