package com.mts.childrenbigbrother.child.dagger

import com.mts.childrenbigbrother.child.services.LocationUpdateForegroundService
import com.mts.childrenbigbrother.child.viewModel.ActionsViewModel
import com.mts.childrenbigbrother.child.viewModel.LoginViewModel

import dagger.Component
import javax.inject.Singleton

/**
 * Created by athanasios.moutsioul on 23/02/2018.
 */
@Singleton
@Component(modules = [AppModule::class, RetrofitModule::class])

interface AppComponent {

    fun inject(target: LoginViewModel)
    fun inject(target: ActionsViewModel)
    fun inject(target: LocationUpdateForegroundService)



}