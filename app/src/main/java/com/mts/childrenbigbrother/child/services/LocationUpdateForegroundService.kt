package com.mts.childrenbigbrother.child.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.mts.childrenbigbrother.child.R
import com.mts.childrenbigbrother.child.view.HomeActivity
import android.os.Build.VERSION_CODES.O
import android.os.Build.VERSION.SDK_INT
import android.os.Looper
import android.support.test.orchestrator.junit.BundleJUnitUtils.getResult
import org.junit.experimental.results.ResultMatchers.isSuccessful
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.OnCompleteListener
import android.os.Looper.myLooper
import com.google.android.gms.location.*
import com.mts.childrenbigbrother.child.application.ChildrenBigBrotherApp
import com.mts.childrenbigbrother.child.model.ChildLocationUpdates
import com.mts.childrenbigbrother.child.model.RetrofitErrorResponse
import com.sonin.soninandroidblankproject.networking.NetworkRequests
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.toast
import javax.inject.Inject


class LocationUpdateForegroundService : Service() {

    @Inject
    lateinit var netService: NetworkRequests

    private val TAG_FOREGROUND_SERVICE              = "FOREGROUND_SERVICE"
    private val mNotificationManager                : NotificationManager?  = null
    private val CHANNEL_ID                          = "channel_01"
    private var mLocationRequest                    : LocationRequest? = null
    private var mLocation                           : Location? = null
    private var compositeDisposable                 = CompositeDisposable()

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationCallback: LocationCallback? = null
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2

    override fun onCreate() {
        super.onCreate()
        ChildrenBigBrotherApp.instance.retrofitComponent?.inject(this)

        Log.d(TAG_FOREGROUND_SERVICE, "My foreground service onCreate().")

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.app_name)
            // Create the channel for the notification
            val mChannel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager?.createNotificationChannel(mChannel)
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                onNewLocation(locationResult!!.lastLocation)
            }
        }

        createLocationRequest()
        getLastLocation()
        try {
            mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper())
        } catch (unlikely: SecurityException) {

            Log.e(TAG_FOREGROUND_SERVICE, "Lost location permission. Could not request updates. $unlikely")
        }


    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val notificationIntent = Intent(this, HomeActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0)

        val notificationBuilder = NotificationCompat.Builder(this)
                .setContentTitle("Location updated are enabled")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pendingIntent)


        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID) // Channel ID
        }

        startForeground(1, notificationBuilder.build())

        return Service.START_NOT_STICKY
    }

    override fun onBind(p0: Intent?): IBinder {

        throw  UnsupportedOperationException("Not yet implemented")
    }


    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()

        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates  than this value.
        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)


    }

    private fun getLastLocation() {
        try {
            mFusedLocationClient?.getLastLocation()
                    ?.addOnCompleteListener { task ->
                        if (task.isSuccessful && task.result != null) {
                            mLocation = task.result
                        } else {
                            Log.w(TAG_FOREGROUND_SERVICE, "Failed to get location.")
                        }
                    }
        } catch (unlikely: SecurityException) {
            Log.e(TAG_FOREGROUND_SERVICE, "Lost location permission.$unlikely")
        }

    }

    private fun onNewLocation(location: Location) {

        with(ChildrenBigBrotherApp.user.child){

            compositeDisposable.add(
                    netService.sendTransitionUpdate(ChildLocationUpdates(codes,id!!,location.latitude.toString(),location.longitude.toString(), System.currentTimeMillis()/1000L))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe ({
                                result ->

                                println("Firebase"+result)

                            }, { error ->

                                RetrofitErrorResponse.onError(error)?.let { println(it)}
                                error.printStackTrace()
                            })
            )
        }







    }
}