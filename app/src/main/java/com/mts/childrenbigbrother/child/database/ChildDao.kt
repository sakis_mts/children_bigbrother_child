package com.mts.childrenbigbrother.child.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.mts.childrenbigbrother.child.model.Child

@Dao
interface ChildDao {

    @Query("select * from Child")
    fun getChild(): LiveData<Child>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addChild(parent: Child)

    @Delete
    fun deleteChild(child: Child)

    @Query("DELETE FROM Child")
    fun deleteChild()
}